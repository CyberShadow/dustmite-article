from lxml import etree
import xml.etree.cElementTree as ET
import sys

border_size = 10


def add_border(svg) -> str:
    # ns = {"svg": "http://www.w3.org/2000/svg"}
    ET.register_namespace('', "http://www.w3.org/2000/svg")
    ET.register_namespace('xlink', "http://www.w3.org/1999/xlink")
    root = ET.fromstring(svg)
    # tree = ET.ElementTree(root)
    # parent_map = {c: p for p in tree.iter() for c in p}
    newroot = ET.fromstring('<svg/>')

    newroot.attrib['width'] = str(int(root.attrib['width']) + border_size * 2)
    newroot.attrib['height'] = str(int(root.attrib['height']) + border_size * 2)

    # to_remove = []
    # defs = None
    # for c in root:
    #     print(c.tag)
    #     if c.tag == '{http://www.w3.org/2000/svg}defs':
    #         defs = c
    #     else:
    #         g.append(c)
    #         to_remove += [c]
    # for c in to_remove:
    #     root.remove(c)

    parser = etree.XMLParser(remove_blank_text=True)
    defs = ET.fromstring(etree.tostring(etree.XML('''
<defs>
<filter id="dropshadow">
  <feGaussianBlur in="SourceAlpha" stdDeviation="5"/> <!-- stdDeviation is how much to blur -->
  <!-- <feOffset dx="0" dy="0" result="offsetblur"/> how much to offset -->
  <feMerge>
    <feMergeNode/> <!-- this contains the offset blurred image -->
    <feMergeNode in="SourceGraphic"/> <!-- this contains the element that the filter is applied to -->
  </feMerge>
</filter>
    </defs>
''', parser=parser)))

    newroot.append(defs)
    g = ET.fromstring('<g/>')
    g.attrib['style'] = "filter:url(#dropshadow)"
    g.attrib['transform'] = 'translate(%d %d)' % (border_size, border_size)
    g.append(root)
    newroot.append(g)

    root = newroot
    xml_str = ET.tostring(root).decode()
    return xml_str


open(sys.argv[2], "w", encoding="utf-8").write(
    add_border(open(sys.argv[1], encoding="utf-8").read())
)
