import std.algorithm.comparison;
import std.algorithm.iteration;
import std.conv;
import std.file;
import std.format;
import std.stdio;

import ae.utils.funopt;
import ae.utils.json;
import ae.utils.main;
import ae.utils.xmlbuild;

struct Node
{
	bool dead;
	@JSONName("new") bool new_;
	bool wasDead;
	string head;
	Node[] children;
	string tail;
}

enum SVG_W = 190;
enum SVG_H = 190;
enum PAD = 10;
enum NODE_R = 2;

void drawTree(string inPath, string outPath, bool showDead = false, bool preview = false)
{
	auto root = inPath.readText.jsonParse!Node();
	size_t getMaxDepth(ref Node n) { return 1 + n.children.map!getMaxDepth.fold!max(size_t(0)); }
	auto maxDepth = getMaxDepth(root);

	auto svg = newXml().svg();
	if (preview)
	{
		svg["xmlns"] = "http://www.w3.org/2000/svg";
		// svg["xmlns:xlink"] = "http://www.w3.org/1999/xlink";
		svg["version"] = "1.1";
		svg["width"] = SVG_W.text;
		svg["height"] = SVG_H.text;
	}

	if (preview)
		svg.rect(["width" : SVG_W.text, "height" : SVG_H.text, "fill" : "#000"]);

	auto layerH = double(SVG_H - PAD * 2) / maxDepth;

	XmlBuildNode[] edges, nodes;
	foreach (dead; 0 .. 2)
		edges ~= svg.path([
			"stroke" : dead ? "#f00" : "#fff",
		]);
	string[2] edgePath;
	foreach (dead; 0 .. 2)
		nodes ~= svg.g([
			"fill" : dead ? "#f00" : "#fff",
		]);

	void drawTree(ref Node n, double y, double x0, double x1, double px, double py, bool dead)
	{
		if (n.wasDead)
			return;
		if (!showDead && n.dead)
			return;
		dead |= n.dead;

		auto x = (x0 + x1) / 2;
		if (px == px)
			edgePath[dead] ~= format!"M %s,%s L %s,%s "
				(px, py, x, y);
		nodes[dead].circle([
			"cx" : x.text,
			"cy" : y.text,
			"r"  : NODE_R.text,
		]);

		if (n.children.length)
		{
			size_t countNodes(ref Node n) { return 1 + n.children.map!countNodes.sum; }
			auto totalNodes = countNodes(n) - 1;

			auto w = x1 - x0;
			auto cx = x0;
			foreach (i, ref c; n.children)
			{
				auto cw = w * countNodes(c) / totalNodes;
				drawTree(c, y + layerH, cx, cx + cw, x, y, dead);
				cx += cw;
			}
		}
	}
	drawTree(root, PAD, PAD, SVG_W - PAD, double.nan, double.nan, false);

	foreach (dead; 0 .. 2)
		edges[dead].d = edgePath[dead];

	svg.toPrettyString().toFile(outPath);
}

mixin main!(funopt!drawTree);
