#!/bin/bash
# shellcheck disable=SC2002
set -eu

last_ok=

frames=()
durs=()
total_dur=0

for d in src.trace/*
do
	if [[ ! -f "$d"/example.d ]]
	then
		continue
	fi

	src_ansi=$d.src.ansi
	if [[ ! -f "$src_ansi" ]]
	then
		hlcat "$d"/example.d > "$src_ansi"
	fi

	src_png=$d.src.png
	if [[ ! -f "$src_png" ]]
	then
		./render_ansi "$src_ansi" "$src_png"
	fi

	dmd_ansi=$d.dmd.ansi
	if [[ ! -f "$dmd_ansi" ]]
	then
		( env -C "$d" dmd -color=on -run example.d 2>&1 || true ) > "$dmd_ansi"
		rm -f "$d"/example
	fi

	dmd_wrap=$d.dmd.wrap
	if [[ ! -f "$dmd_wrap" ]]
	then
		< "$dmd_ansi" ./word_wrap | ./word_wrap > "$dmd_wrap"
	fi

	dmd_png=$d.dmd.png
	if [[ ! -f "$dmd_png" ]]
	then
		./render_ansi "$dmd_wrap" "$dmd_png"
	fi

	[[ "$d" == *-1 ]] && ok=true || ok=false

	combined_png=$d.combined.png
	if [[ ! -f "$combined_png" ]]
	then
		if [[ -z "$last_ok" ]]
		then
			last_src_png=$src_png
		else
			last_src_png=$last_ok.src.png
		fi
			
		./combine "$src_png" "$last_src_png" "$dmd_png" "$combined_png"
	fi

	combined_svg=$d.combined.svg
	if [[ ! -f "$combined_svg" ]]
	then
		convert "$combined_png" -type TrueColorAlpha -alpha on bmp:- |
			monocre read --output-format=svg ~/work/monocre/urxvt.json > "$combined_svg"
	fi

	tree_json=$d.json
	tree_svg=$d.tree.svg
	if [[ ! -f "$tree_svg" ]]
	then
		./drawtree --show-dead "$tree_json" "$tree_svg"
	fi

	frames+=("$combined_svg")
	$ok && dur=1000 || dur=500

	durs+=("$dur")
	total_dur=$((total_dur+dur))

	if $ok
	then
		combined_ok_png=$d.combined_ok.png
		if [[ ! -f "$combined_ok_png" ]]
		then
			./combine "$src_png" "$src_png" "$dmd_png" "$combined_ok_png"
		fi

		combined_ok_svg=$d.combined_ok.svg
		if [[ ! -f "$combined_ok_svg" ]]
		then
			convert "$combined_ok_png" -type TrueColorAlpha -alpha on bmp:- |
				monocre read --output-format=svg ~/work/monocre/urxvt.json > "$combined_ok_svg"
		fi

		tree_ok_svg=$d.tree_ok.svg
		if [[ ! -f "$tree_ok_svg" ]]
		then
			./drawtree "$tree_json" "$tree_ok_svg"
		fi

		frames+=("$combined_ok_svg")
		dur=1000
		if [[ "$d" == src.trace/00000058-#00000042-1 ]]
		then
			dur=10000
		fi

		durs+=("$dur")
		total_dur=$((total_dur+dur))

		last_ok=$d
	fi

	if [[ "$d" == src.trace/00000058-#00000042-1 ]]
	then
		break
	fi
done

{
	head -1 "${frames[0]}"
	echo '<defs>'
	echo '<path id="y" fill="#0f0" transform="translate(269 244) scale(20)" d="m 0 4 l 1 -1 l 1 1 l 3 -3 l 1 1 l -4 4 z"/>'
	echo '<path id="n" fill="#f00" transform="translate(269 244) scale(20)" d="m 1 0 l 2 2 l 2 -2 l 1 1 l -2 2 l 2 2 l -1 1 l -2 -2 l -2 2 l -1 -1 l 2 -2 l -2 -2 z"/>'
	echo '</defs>'
	echo '<rect width="10000" height="10000" fill="#000"/>'

	for ((i = 0; i < ${#frames[@]} ; i++))
	do
		d=${frames[$i]}

		[[ "$d" == *-1.* ]] && ok=true || ok=false

		$ok && def=y || def=n

		echo '<g transform="translate(0 3)" style="display: none; font-family: monospace; text-anchor: middle; dominant-baseline: middle;">';
		grep '</\?text' "$d"
		echo '<use href="#'$def'"/>'
		echo '<g transform="translate(209 0)">'
		< "${d/combined/tree}" head -n -1 | tail -n +2
		echo '</g>'
		echo -n '<animate attributeName="display" calcMode="discrete" values="none;inline;none;none" keyTimes="0;'
		t=0
		for ((j = 0; j <= ${#frames[@]} ; j++))
		do
			if (( j == i || j == i + 1 ))
			then
				if [[ $t -eq "$total_dur" ]]
				then
					printf '1;'
				else
					p=$((t * 1000000000 / total_dur))
					printf '0.%09d;' "$p"
				fi
			fi
			if [[ $j -lt ${#frames[@]} ]]
			then
				dur=${durs[$j]}
				t=$((t+dur))
			fi
		done
		printf '1" dur="%dms" repeatCount="indefinite" />' "$total_dur"
		echo '</g>'
	done
	echo '</svg>'
} > anim.svg

svgo --disable=removeHiddenElems anim.svg
