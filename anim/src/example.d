import std.stdio;

void main()
{
    int pointless = 17;
    int var = 1 * 21 + 0;

    if (var > 0)
        var *= 2;
    if (var < 0)
        var += pointless;

    writeln(var);
}
