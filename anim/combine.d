import std.file;
import std.math;
import std.stdio;

import ae.utils.array;
import ae.utils.funopt;
import ae.utils.graphics.color;
import ae.utils.graphics.hls;
import ae.utils.graphics.image;
import ae.utils.graphics.libpng;
import ae.utils.graphics.view;
import ae.utils.main;

// void main()
// {
// 	string lastGood;
// 	foreach (de; dirEntries("src.trace", SpanMode.shallow))
// 		if (de.isDir)
// 		{
// 			auto src = de ~ ".src.png";
// 			auto lastGoodSrc = lastGood ~ ".src.png";
// 			auto dmd = de ~ ".dmd.png";
// 			if (src.exists)
// 				combine(src, lastGood ? lastGoodSrc : src, dmd, de ~ ".combined.png");
// 		}
// }

auto readSrc(string fn)
{
	return fn.read
		.bytes
		.decodePNG!RGB()
		.crop(7, 2, 406, 184);
}

auto readDmd(string fn)
{
	return fn.read
		.bytes
		.decodePNG!RGB()
		.crop(7, 2, 406, 184);
}

// ushort flipL(ushort l)
// {
// 	enum g = 1.2;
// 	return cast(ushort)(pow(1 - pow(l / 240.0, 1/g), g) * 240);
// }

void combine(string srcNewFn, string srcOldFn, string dmdFn, string outFn)
{
	auto srcNew = srcNewFn.readSrc;
	auto srcOld = srcOldFn.readSrc;
	auto dmd    =    dmdFn.readDmd;

	auto line = Image!RGB(srcNew.w, 13);
	line.pixels[] = [RGB.black];
	line.scanline(6)[] = [RGB.white];

	auto v = vjoiner([srcOld.copy, line, dmd.copy]);

	HLS!RGB hls;
	foreach (y; 0 .. v.h)
		foreach (x; 0 .. v.w)
		{
			ushort h,l,s;
			hls.toHLS(v[x, y], h, l, s);
			// l = flipL(l);
			// l = cast(ushort)(240 - l);
			// if (h == 120)
			// 	l -= 90;
			if (y < srcNew.h && srcNew[x, y] != srcOld[x, y])
			{
				h = 0;
				l = 45; //(l + 120 * 1) / 2;
				s = 240;
			}
			v[x, y] = hls.fromHLS(h, l, s);
		}

	v.toPNG.toFile(outFn);
}

mixin main!(funopt!combine);
