- [ ] describe internals
  - [X] trees
  - [X] optimization
  - [X] strategies
    - [X] (reduction_i, result) => reduction_i+1
    - [ ] maybe move this to a section which is more tangential to strategies, such as multithreading
  - [ ] multithreading
    - [X] "highly sophisticated predictor"
    - [ ] This limitation also prevented implementing features such as "white-out" mode.

- [X] describe new optimizations

  - [X] COW trees
    - [X] no longer a global
  - [X] polyhashing
  - [X] addresses
    - [X] addresses need to be stable, therefore tombstones
  - [X] bulking contiguous writes
  - [X] lockingBinaryWriter
  - [X] add graph with my benchmarks
    - [X] after a few more rounds of applying Walter Bright's secret weapon

  - [ ] add --white-out screenshot (abridged) + link to animated version

- [ ] more uses
  - reduce a diff (diff mode)
  - reduce a commit list
  - alternative to code coverage
    - Just because a line of code is executed, that doesn't mean it's *necessary*.
- [ ] conclusion
  - dustmite is readily available in major distributions 
    - but probably not the version with the latest improvements
    - having a favorite tool one `apt-get` / `pacman -S` away is a 

----------------------------------------------

- [ ] fixes
  - [X] The problem was so bad, that at high core counts, lookahead mode it was even slower than
  - [ ] maybe something about why only a 3x speedup
        "A mere threefold speed-up from a 32-fold increase in computational power may seem underwhelming."
    - every case is different
    - expected speedup is x log2(cores)-1 
    - 
