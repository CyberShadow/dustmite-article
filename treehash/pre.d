import std.digest.md;
import std.file;
import std.net.curl;
import std.path;
import std.regex;
import std.stdio;
import std.string;

import ae.net.ietf.url;
import ae.sys.file;
import ae.utils.digest;
import ae.utils.regex;

void main()
{
	auto dot = cast(string)readFile(stdin);
	dot = dot.replaceAll!(
		(c)
		{
			auto eq = c[1];
			// eq = eq.replaceAll!(c => `\` ~ c[0])(re!`[{}]`);
			auto hash = getDigestString!MD5(eq).toLower;
			auto fn = `tex/` ~ hash ~ `.png`;
			foreach (ext; ["png", "svg"])
			{
				auto url = "https://latex.codecogs.com/" ~ ext ~  ".latex?" ~ encodeUrlParameter(eq);
				auto extfn = fn.setExtension("." ~ ext);
				if (!extfn.exists)
				{
					stderr.writefln("Downloading %s for: %s", ext, eq);
					download(url, extfn);
				}
			}

			return `<table border="0"><tr><td><img src="` ~ fn ~ `"/></td></tr></table>`;
		})(regex(`\$([^$]*)\$`, "gm"));
	stdout.rawWrite(dot);
}
