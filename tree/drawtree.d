import core.bitop;

import std.algorithm.comparison;
import std.algorithm.iteration;
import std.algorithm.searching;
import std.array;
import std.conv;
import std.format;
import std.random;
import std.stdio;
import std.traits;

import ae.utils.array;
import ae.utils.xmlbuild;

enum LAYERS = 5;
enum LAYER_H = 32;
enum W = 1 << 9;
enum H = LAYERS * LAYER_H;
enum PAD = 20;
enum SVG_W = W + PAD * 2;
enum SVG_H = H + PAD * 2;
enum COLOR_DEP = "#088";
enum EDIT_DX = 0;
enum EDIT_DY = LAYER_H;

enum Mode
{
	normal,
	edit,
	address,
}

alias Node = uint;
uint depth(Node n) { return bsr(n); }
uint pos(Node n) { return n ^ (1 << depth(n)); }
Node node(int pos, int depth) { return (1 << depth) | pos; }

bool isParentOf(Node p, Node c) { return p.depth > c.depth ? false : c >> (c.depth - p.depth) == p; }

void draw(Mode mode)
{
	auto svgW = SVG_W;
	auto svgH = SVG_H;
	if (mode == Mode.edit)
	{
		svgW += EDIT_DX;
		svgH += EDIT_DY;
	}

	auto svg = newXml().svg();
	svg["xmlns"] = "http://www.w3.org/2000/svg";
	svg["xmlns:xlink"] = "http://www.w3.org/1999/xlink";
	svg["version"] = "1.1";
	svg["width"] = svgW.text;
	svg["height"] = svgH.text;
	svg["viewBox"] = "0.5 0.5 %d %d".format(svgW, svgH);
	svg["font-family"] = "Arial, Helvetica, sans-serif";
	svg["font-size"] = "12px";

	auto defs = svg.defs();
	if (mode == Mode.normal)
	{
		defs.path([
			"id" : "head",
			"stroke" : "black",
			"fill" : "none",
			"stroke-linecap" : "square",
			// "d" : "M -5 -6 h 10 v 12 h -10 z m 3 3 v 6 m 0 -3 h 4 m 0 -3 v 6",
			"d" : "M -2 -2 v 4 m 0 -2 h 4 m 0 -2 v 4",
		]);
		defs.path([
			"id" : "tail",
			"stroke" : "black",
			"fill" : "none",
			"stroke-linecap" : "square",
			// "d" : "M -5 -6 h 10 v 12 h -10 z m 3 3 h 4 m -2 0 v 6",
			"d" : "M -2 -2 h 4 m -2 0 v 4",
		]);
	}

	svg.rect(["width" : svgW.text, "height" : svgH.text, "fill" : "#fff"]);
	auto deps = svg.path();

	auto rule = ("11443343" ~ "43332244" ~ "2233222222221111").map!(c => [c].to!int).array;
	auto htRule = ("0" ~ "33" ~ "0132" ~ "12312119" ~ "3012211333100022" ~ "99329919199999233333333333333333").map!(c => [c].to!int).array;

	// int[2][] edits = [[4, 3], [3, 2]];
	Node[] edits = [node(6, 3), node(5, 4), node(9, 4)];

	int[] treeX, treeY;
	size_t getTreeIndex(Node n)
	{
		if (mode == Mode.normal)
			return 0;
		else
			return edits.any!(edit => n.isParentOf(edit)) ? 1 : 0; // TODO
	}

	if (mode == Mode.edit)
	{
		treeX = [0, EDIT_DX];
		treeY = [EDIT_DY, 0];
	}
	else
		treeX = treeY = [0];

	int xOf(Node n) { return PAD + (n.pos * 2 + 1) * (W / (2 << n.depth)); }
	int yOf(Node n) { return PAD + LAYER_H * n.depth; }

	if (mode == Mode.normal)
	{
		string depsPath;
		void drawDependency(Node n0, Node n1)
		{
			svg.circle([
				"cx" : xOf(n0).text,
				"cy" : yOf(n0).text,
				"r" : 10.text,
				"fill" : COLOR_DEP,
			]);
			depsPath ~= format(
				"M %d,%d L %d,%d",
				xOf(n0), yOf(n0),
				xOf(n1), yOf(n1),
			);
		}
		drawDependency(node(1, 4), node(1, 3));
		drawDependency(node(6, 3), node(9, 4));
		drawDependency(node(6, 3), node(5, 4));

		deps["d"] = depsPath;
		deps["stroke"] = COLOR_DEP;
		deps["stroke-width"] = "5";
		deps["stroke-linecap"] = "round";
	}

	auto oldTree = svg.path(); // old - faded

	string treePath, oldTreePath, newTreePath, oldNewPath;
	void drawSubtree(Node n, int px, int py, int copy)
	{
		auto nodeColor = "#888";
		string* target = &treePath;

		size_t treeIndex;
		if (mode == Mode.edit)
		{
			if (copy == 0) // old tree
			{
				treeIndex = 0;
				nodeColor = "#ddd";
				target = &oldTreePath;
			}
			else // main (solid) tree
			{
				treeIndex = getTreeIndex(n);
				if (n > 1 && treeIndex != getTreeIndex(n >> 1))
					target = &oldNewPath;
				else if (treeIndex == 1)
				{
					target = &newTreePath;
					nodeColor = "#080";
				}
				if (edits.canFind(n))
					nodeColor = "#F00";
			}
		}
		else
		{
			treeIndex = 0;
			if (copy != 1)
				return;
		}

		auto x = xOf(n) + treeX[treeIndex];
		auto y = yOf(n) + treeY[treeIndex];

		if (px || py)
		{
			*target ~= format(
				"M %d,%d L %d,%d ",
				px, py, x, y,
			);
		}

		if (mode == Mode.normal)
		{
			auto dx = n.depth < 3 ? 5 : n.depth == 5 ? 3 : 10;
			auto dy = n.depth < 3 ? -10 : n.depth == 5 ? 10 : 0;
			auto ht = htRule[(1 << n.depth) - 1 + n.pos];
			if (ht & 1) svg.use(["xlink:href" : "#head", "x" : (x - dx).text, "y" : (y + dy).text]);
			if (ht & 2) svg.use(["xlink:href" : "#tail", "x" : (x + dx).text, "y" : (y + dy).text]);
		}

		if (mode == Mode.address)
		{
			svg.circle(["cx" : x.text, "cy" : y.text, "r" :  8.text, "fill" : "black"]);
			svg.circle(["cx" : x.text, "cy" : y.text, "r" :  7.text, "fill" : "white"]);
			svg.text(["x" : x.text, "y" : (y + 4).text, "text-anchor" : "middle"])[] = n == 1 ? "−" : text(n & 1);
		}
		else
		{
			svg.circle([
				"cx" : x.text,
				"cy" : y.text,
				"r" : 5.text,
				"fill" : nodeColor,
			]);
		}
		if (n.depth < LAYERS)
			foreach (i; 0 .. 2)
			{
				auto n1 = (n << 1) | i;
				auto subruleLength = rule.length >> n1.depth;
				auto subrule = rule[n1.pos * subruleLength .. $][0 .. subruleLength];
				if (subrule.reduce!max < n.depth)
					continue;
				drawSubtree(n1, x, y, copy);
			}
	}
	drawSubtree(1, 0, 0, 0);

	auto tree    = svg.path(); // main - solid
	auto newTree = svg.path(); // transition - dashed 
	auto oldNew  = svg.path(); // transition - dashed 
	drawSubtree(1, 0, 0, 1);
	

	oldTree["d"] = oldTreePath;
	oldTree["stroke"] = "#ccc";
	oldTree["stroke-width"] = "5";
	oldTree["stroke-linecap"] = "round";

	tree["d"] = treePath;
	tree["stroke"] = "black";
	tree["stroke-width"] = "5";
	tree["stroke-linecap"] = "round";

	newTree["d"] = newTreePath;
	newTree["stroke"] = "#060";
	newTree["stroke-width"] = "5";
	newTree["stroke-linecap"] = "round";
	
	oldNew["d"] = oldNewPath;
	oldNew["stroke"] = "black";
	oldNew["stroke-width"] = "5";
	oldNew["stroke-dasharray"] = "0,7.5";
	oldNew["stroke-linecap"] = "round";

	svg.toString().toFile("tree-%s.svg".format(mode));
}

void main()
{
	foreach (m; EnumMembers!Mode)
		draw(m);
}
