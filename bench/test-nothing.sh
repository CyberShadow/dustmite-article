#!/bin/bash
set -eu

diff <(find . -type f | sort | xargs md5sum | md5sum) <(echo '9a9bf19194b909ae28eb4b9386fccd8a  -')
