#!/bin/bash
set -eu

numlines=$(wc -l < log.txt)

lines=100
while [[ "$lines" -lt "$numlines" ]]
do
	yes=$(head -n "$lines" log.txt | grep -c '=> Yes')
	no=$(head -n "$lines" log.txt | grep -c '=> No')
	printf '%d: %d\n' "$lines" "$((yes*1000/no))"
	lines=$((lines+100))
done
