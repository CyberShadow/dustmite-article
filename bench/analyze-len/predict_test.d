import ae.utils.math;

import std.algorithm.searching;
import std.container.rbtree;
import std.file;
import std.math;
import std.range;
import std.stdio;
import std.string;

bool[] results;

void main()
{
	foreach (line; "log.txt".readText.splitLines)
		if (line.canFind(" => No"))
			results ~= false;
		else
		if (line.canFind(" => Yes"))
			results ~= true;
	results = results[0 .. 2000];

	auto avg = double(results.count!"a") / results.length;
	foreach (c; [0, 1, 0.5, 0.1, avg])
		writefln("%s - Constant (%s)", predict(ConstantPredictor(c)), c);
	// foreach (w; 1 .. results.length)
	// 	writefln("%s - Window (%d)", predict(WindowPredictor(w)), w);
	// foreach (w; 500 .. 700)
	// 	if (w % 50 == 0)
	// 	foreach (e; iota(30.0, 40.0, 1.0))
	// 		writefln("%s - Dirty Weighted Window (%d, %s)", predict(DirtyWeightedWindowPredictor(w, e)), w, e);
	// foreach (w; iota(600, 1000, 50))
	// 	foreach (e; iota(40.0, 60.0, 2.0))
	// 		writefln("%s - Weighted Window (%d, %s)", predict(WeightedWindowPredictor(w, e)), w, e);
	foreach (e; iota(0.0, 0.1, 0.001))
		writefln("%s - Accumulating Predictor (%s)", predict(AccumulatingPredictor(e)), e);
}

enum numThreads = 48;

double predict(Predictor)(Predictor predictor)
{
	size_t pos = 0;
	size_t time;
	while (pos < results.length)
	{
		// Simulate DustMite logic
		// Assume all tests execute with the same speed,
		// and effectively all lookahead jobs finish during every step

		static struct PredictedState
		{
			double probability;
			Predictor predictor;
			bool resolved;
			PredictedState*[2] children;
		}

		auto initialState = new PredictedState(1.0, predictor);
		alias PredictionTree = RedBlackTree!(PredictedState*, (a, b) => a.probability > b.probability, true);
		auto predictionTree = new PredictionTree((&initialState)[0..1]);

		foreach (thread; 0 .. numThreads)
		{
			if (predictionTree.empty)
				break;

			auto state = predictionTree.front;
			predictionTree.removeFront();

			state.resolved = true;

			double prediction = state.predictor.predict();

			foreach (outcome; 0 .. 2)
			{
				auto probability = outcome ? prediction : 1 - prediction;
				if (probability == 0)
					continue; // no chance
				probability *= state.probability; // accumulate
				auto nextState = new PredictedState(probability, state.predictor);
				state.children[outcome] = nextState;
				nextState.predictor.put(!!outcome);
				predictionTree.insert(nextState);
			}
		}

		// Now jump forward as far as predictions have allowed
		auto state = initialState;
		while (state && state.resolved && pos < results.length)
		{
			auto outcome = results[pos++];
			// debug writef("%d -> ", outcome);
			predictor.put(outcome);
			state = state.children[outcome];
		}
		// debug writefln("!");

		// Only one time unit has passed for all successfully predicted jobs.
		time++;
	}

	return double(time) / results.length;
}

struct ConstantPredictor
{
	double probability;

	void put(bool outcome) {}
	double predict() { return probability; }
}

struct WindowPredictor
{
	bool[] window;
	size_t pos, numTotal, numPositive;
	this(size_t size) { window.length = size; }

	void put(bool outcome)
	{
		numPositive -= window[pos];
		window[pos++] = outcome;
		numPositive += outcome;
		if (numTotal < pos)
			numTotal = pos;
		if (pos == window.length)
			pos = 0;
	}

	double predict() { return numTotal == 0 ? 0.5 : double(numPositive) / numTotal; }
}

struct DirtyWeightedWindowPredictor
{
	bool[] window;
	size_t pos;
	double exp;
	this(size_t size, double exp = 1) { window.length = size; this.exp = exp; }

	void put(bool outcome)
	{
		window[pos++] = outcome;
		if (pos == window.length)
			pos = 0;
	}

	double predict()
	{
		double totalProb = 0, totalWeight = 0;
		auto p = pos;
		foreach (i; 0 .. window.length)
		{
			auto weight = (double(i + 1) / window.length) ^^ exp;
			totalProb += window[p++] * weight;
			if (p == window.length)
				p = 0;
			totalWeight += weight;
		}
		return totalProb / totalWeight;
	}
}

struct WeightedWindowPredictor
{
	bool[] window;
	size_t pos, numTotal;
	double exp;
	this(size_t size, double exp = 1) { window.length = size; this.exp = exp; }

	void put(bool outcome)
	{
		window[pos++] = outcome;
		if (numTotal < pos)
			numTotal = pos;
		if (pos == window.length)
			pos = 0;
	}

	double predict()
	{
		if (!numTotal)
			return 0.5;
		double totalProb = 0, totalWeight = 0;
		foreach (i; (window.length - numTotal) .. window.length)
		{
			auto p = (pos + i) % window.length;
			auto weight = (double(i + 1) / window.length) ^^ exp;
			totalProb += window[p++] * weight;
			totalWeight += weight;
		}
		return totalProb / totalWeight;
	}
}

struct AccumulatingPredictor
{
	double r = 0.5;
	double exp;
	this(double exp) { this.exp = exp; }

	void put(bool outcome)
	{
		r = (1 - exp) * r + exp * outcome;
	}

	double predict()
	{
		return r;
	}
}
