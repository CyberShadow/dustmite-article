#!/bin/bash
set -eu

dmd -o- -I~/work mainfile.d 2>&1 | grep -F 'unknown, please file report'
