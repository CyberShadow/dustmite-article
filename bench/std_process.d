module std_process;

import ae.sys.datamm;
import ae.utils.array;
import ae.utils.path;

import core.thread;
import core.time;

import std.algorithm.sorting;
import std.array;
import std.datetime.stopwatch;
import std.digest.crc;
import std.exception;
import std.file;
import std.process;
import std.stdio;
import std.typecons;

public import std.process : escapeShellFileName, Config;

class Pid
{
private:
	MonoTime endTime;
	int status;
}

auto tryWait(Pid pid)
{
	alias R = Tuple!(bool, "terminated", int, "status");
	auto now = MonoTime.currTime;
	return R(now >= pid.endTime, pid.status);
}

int wait(Pid pid)
{
	auto remaining = pid.endTime - MonoTime.currTime;
	if (remaining > Duration.zero)
		Thread.sleep(remaining);
	return pid.status;
}

enum execCacheDir = "/home/vladimir/work/DustMite/-/2020-03-17/bench/exec-cache/";

Pid spawnShell(string command, File stdin, File stdout, File stderr, typeof(null) env, Config config, string pwd)
{
	CRC32 hash;
	hash.put(command.bytes);
	foreach (de; pwd.dirEntries(SpanMode.depth).array.sort)
		if (de.isFile)
		{
			hash.put(de.name.fastRelativePath(pwd).bytes);
			if (de.size > 0)
			{
				auto m = mapFile(de.name, MmMode.read);
				hash.put(m.contents.bytes);
			}
		}
	auto digest = hash.finish();
	string cacheFn = execCacheDir ~ digest.toHexString().assumeUnique;
	auto now = MonoTime.currTime();
	static struct CacheEntry
	{
		Duration duration;
		int status;
	}

	CacheEntry ce;
	if (cacheFn.exists)
	{
		auto m = mapFile(cacheFn, MmMode.read);
		ce = (cast(CacheEntry[])m.contents)[0];
	}
	else
	{
		StopWatch sw;
		sw.start();
		auto pid = std.process.spawnShell(command, stdin, stdout, stderr, null, config, pwd);
		auto status = std.process.wait(pid);
		sw.stop();
		ce = CacheEntry(sw.peek(), status);
		std.file.write(cacheFn, (&ce)[0..1]);
	}
	auto pid = new Pid;
	pid.endTime = now + ce.duration;
	pid.status = ce.status;
	return pid;
}

Pid spawnShell(string command, typeof(null) env, Config config, string pwd)
{
	return spawnShell(command, stdin, stdout, stderr, env, config, pwd);
}
