#!/bin/bash
set -eEuo pipefail
shopt -s lastpipe

commits=(
	d501228fed930defc0968ddec06c428c0a63216c # last commit before March optimizations
	# 4d361cb # dustmite: Use templates instead of delegates for dump
	772a8fb # dustmite: Use lockingBinaryWriter
	621991b # dustmite: Bulk adjacent writes
	1f1f732 # splitter: Don't dereference enum AAs at runtime
	3fea926 # dustmite: Mark final classes as such
	2ca0522 # Overhaul tree representation and edit algorithms
	# d9da7cf # dustmite: Fail no-op concat reductions
	# ad4124f # dustmite: Start a new iteration after a successful Concat reduction
	# f197986 # dustmite: Get rid of the "root" global
	8b5f639 # dustmite: Handle inapplicable reductions in lookahead
	bf407bc # dustmite: Make descendant recounting incremental
	9eb4126 # dustmite: Move lookahead saving and process creation into worker thread
	5b80b03 # Unify incremental and full updates of computed Entity fields
	f85acdf # Switch to incremental polynomial hashing
	575406e # Speed up applyReduction.edit
	23a67fb # Re-optimize incrementally for the Concat reduction
	80b7ba4 # dustmite: Speed up removing dependencies under removed nodes
	# ec81973 # Speed up address comparison
	# 048a0fd # Keep children of removed nodes in the tree
	# 48ed0a5 # dustmite: Make findEntity traverse dead nodes
	# 53d3bf6 # dustmite: Recalculate dead entities recursively too
	# c3d1215 # dustmite: Traverse dead entities when editing them, too
	# 226a651 # dustmite: Do not copy dead entities for editing
	# b8f2844 # Maintain cached cumulative dependents per-node
	# 9f5a4f1 # dustmite: Create less garbage during I/O
	# df752dc # Maintain cached full content of each node
	# 4b165e6 # Revert "Maintain cached full content of each node"
	# 965fbc3 # dustmite: Speed up strategy iteration over dead nodes
	# 15d0a8f # dustmite: Remove use of lazy arguments in address iteration
	# 9505bf6 # dustmite: Fix recursion for dead nodes in recalculate
	# 3a76633 # dustmite: Remove Reduction.target
	# c04c843 # dustmite: Replace Reduction.address with an Address#
	# 6746464 # Add --white-out option
	214d000 # dustmite: Add reduction application cache
	# e859e86 # dustmite: Grow reduction application cache dynamically
	# fd3ad29 # dustmite: Speed up dependency recalculation
	# a10ef7f # dustmite: Fix crash with --whiteout + --trace

	# 256a651 # dustmite: Speed up dumping
	# df42f62 # dustmite: Add --max-steps
	# 886c6f2 # dustmite: Make measure's delegate scoped
	# 732d0f1 # dustmite: Add more performance timers
	05acf86 # dustmite: Implement non-linear lookahead prediction
	0a7a937 # dustmite: Improve prediction formula
)

#first_working_lookahead_commit=8b5f639 # strictly speaking
first_working_lookahead_commit=f85acdf # working but extremely slow before this point

repo=$HOME/work/DustMite
tmp=/tmp/2020-03-29

first_working_lookahead_time=$(git -C "$repo" log --format=%at -1 "$first_working_lookahead_commit")
first_allowed_lookahead_time=$first_working_lookahead_time

#########################################################################################

for ((i = 0; i < ${#commits[@]} ; i++))
do
	commit=${commits[$i]}
	commit=$(git -C "$repo" log --format=%H -1 "$commit")
	commits[$i]=$commit
done

function need_build() {
	local commit=$1

	cache_build_fn=cache/build-$commit
	if [[ -x "$cache_build_fn" ]]
	then
		printf 'Using cached build for commit %s\n' "$commit" 1>&2
	else
		printf 'Building commit %s\n' "$commit" 1>&2
		tmp_src=$tmp/dustmite_src
		rm -rf "$tmp_src"
		mkdir "$tmp_src"
		git -C "$repo" archive "$commit" | tar x -C "$tmp_src"
		rm -rf "$tmp_src"/tests
		greplace std.process std_process "$tmp_src"
		cp std_process.d "$tmp_src"/
		(
			source ~/lib/d-ver.sh
			d_ldc
			env -C "$tmp_src" dbuildr --force -g dustmite.d
		)
		cp -a "$tmp_src"/dustmite "$cache_build_fn"
		rm -rf "$tmp_src"
	fi

	cp -a "$cache_build_fn" "$tmp"/dustmite
}

num_lines=200
# num_lines=1500

test=full

function need_sample() {
	local commit=$1
	local j=$2
	local max_samples=$3

	printf 'Collect sample for commit %s and j=%d\n' "$commit" "$j" 1>&2

	local commit_time
	commit_time=$(git -C "$repo" log --format=%at -1 "$commit")

	cache_time_fn=cache/time-$test-$num_lines-$j-$commit_time-$commit.txt
	if [[ -f "$cache_time_fn" && "$(wc -l < "$cache_time_fn")" -ge "$max_samples" ]]
	then
		printf 'Using cache: %s\n' "$cache_time_fn" 1>&2
	elif [[ "$j" -gt 0 && "$commit_time" -lt "$first_allowed_lookahead_time" ]]
	then
		printf 'Too old for lookahead, skipping\n' 1>&2
		dur=-2
		return
	else
		rm -rf "$tmp"
		mkdir "$tmp"
		cp -a src "$tmp"

		need_build "$commit"

		cmdline=(
			dver 2.090.0
			env -C "$tmp"
			./dustmite src "$PWD"/test-"$test".sh
			-j"$j"
		)
		printf 'Testing commit %s (j=%s):\n' "$commit" "$j" 1>&2

		set +o pipefail
		start=$(date +%s%N)
		"${cmdline[@]}" | head -n "$num_lines" | tee "$tmp"/log.txt 1>&2
		end=$(date +%s%N)
		set -o pipefail

		dur=$((end-start))

		if [[ "$(wc -l < "$tmp"/log.txt)" -ne "$num_lines" ]]
		then
			printf 'Bad result!\n'
			dur=-1
			return
		fi

		printf '%d\n' "$dur" >> "$cache_time_fn"
		printf 'Saved to cache: %s\n' "$cache_time_fn" 1>&2
	fi

	# local num_samples
	# num_samples=$(wc -l < "$cache_time_fn")
	dur=$(sort -n "$cache_time_fn" | head -1)
}

nproc=$(nproc)

#########################################################################################
# Collect

# best_j_time=999999999999999999
# for s in $(seq 20)
# do
# 	# for ((j = 0; j <= nproc; j += 4))
# 	for ((j = 24; j <= 48; j += 4))
# 	do
# 		if [[ $j -eq 28 || $j -eq 44 ]]
# 		then
# 			continue
# 		fi

# 		need_sample "${commits[-1]}" "$j" "$s"
# 		dur=$(sort -n "$cache_time_fn" | head -1)
# 		if [[ "$dur" -lt "$best_j_time" ]]
# 		then
# 			best_j_time=$dur
# 			best_j=$j
# 		fi
# 	done
# done
best_j=32

if false
then
	function need_samples() {
		local max_samples=$1
		shift
		local js=("$@")

		local num_samples
		for num_samples in $(seq "$max_samples")
		do
			local j
			for j in "${js[@]}"
			do
				printf 'Collecting up to %d samples with j=%d\n' "$num_samples" "$j" 1>&2
				for commit in "${commits[@]}"
				do
					need_sample "$commit" "$j" "$num_samples"
				done
			done
		done
	}

	need_samples 5 0 "$best_j" # "$nproc"

	first_allowed_lookahead_time=0

	need_samples 1 "$best_j"

	first_allowed_lookahead_time=$first_working_lookahead_time

	need_samples 5 0 "$best_j"
	need_samples 5 "$nproc"

	for _ in $(seq 20)
	do
		for j in $(seq 16 48)
		do
			need_sample "${commits[-1]}" "$j" 5
		done
	done

	need_samples 20 0 "$best_j"

	first_allowed_lookahead_time=0

	need_samples 10 "$best_j"
fi

#########################################################################################
# Print results

for commit in "${commits[@]}"
do
	printf '%s\t' "$commit"
	for j in 0 "$best_j"
	do
		need_sample "$commit" "$j" 1
		printf '%d\t' "$dur"
	done
	git -C "$repo" log --format=%s -1 "$commit"
done > results.txt
