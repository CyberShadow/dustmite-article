import std.algorithm.comparison;
import std.algorithm.iteration;
import std.algorithm.searching;
import std.array;
import std.conv;
import std.file;
import std.math;
import std.range;
import std.stdio;
import std.string;

import ae.utils.xmlbuild;

enum ROW_H = 48;
enum LABEL_LINE_H = 9;
// enum LABEL_COL_W = 1 << 8;
// enum TIMES_COL_W = 1 << 8;
enum SVG_W = 1 << 9;
enum LEGEND_H = 24;

struct Sample
{
	string commit, subject;
	long[] times;
}

void main()
{
	auto samples = "results.txt"
		.readText
		.splitLines
		.map!(line => line.split("\t"))
		.map!(cols => Sample(cols[0], cols[$-1], cols[1..$-1].map!(to!long).array))
		.array;

	auto svgW = SVG_W;
	auto svgH = samples.length * ROW_H + LEGEND_H;

	auto svg = newXml().svg();
	svg["xmlns"] = "http://www.w3.org/2000/svg";
	svg["xmlns:xlink"] = "http://www.w3.org/1999/xlink";
	svg["version"] = "1.1";
	svg["width"] = svgW.text;
	svg["height"] = svgH.text;
	// svg["viewBox"] = "0.5 0.5 %d %d".format(svgW, svgH);

	auto defs = svg.defs();
	auto g1 = defs.linearGradient(["id" : "g1", "x1" : "0%", "y1" : "0%", "x2" : "0%", "y2" : "100%"]);
	g1.stop(["offset" :   "0%", "style" : "stop-color: #0a0"]);
	g1.stop(["offset" : "100%", "style" : "stop-color: #060"]);
	auto g2 = defs.linearGradient(["id" : "g2", "x1" : "0%", "y1" : "0%", "x2" : "0%", "y2" : "100%"]);
	g2.stop(["offset" :   "0%", "style" : "stop-color: #a00"]);
	g2.stop(["offset" : "100%", "style" : "stop-color: #600"]);
	defs.path([
		"id" : "tear",
		"fill" : "#fff",
		"transform" : "scale(8)",
		"d" : "m 1 0 l -1 1 l 1 1 l -1 1 h 1 l 1 -1 l -1 -1 l 1 -1 z",
	]);

	svg.rect(["width" : svgW.text, "height" : svgH.text, "fill" : "#fff"]);

	// auto commitHashes = svg.g([
	// 	"style" : (
	// 		"font-family: monospace;" ~
	// 		"font-size : 8px;"
	// 	),
	// ]);
	// enum COMMIT_HASH_H = 10;
	enum COMMIT_MESSAGE_CHAR_W = 6;
	auto commitDelimiters = svg.path([
		"stroke" : "#ccc",
	]);
	string commitDelimiterPath;
	auto commitMessages = svg.g([
		"style" : (
			"font-family: monospace;" ~
			"font-size : 10px;"
		),
	]);
	auto times = svg.g();
	auto timeLabels = svg.g([
		"style" : (
			"font-family: monospace;" ~
			"font-weight: bold;" ~
			"font-size : 16px;"
		),
	]);
	auto tears = svg.g();

	auto mins = samples.front.times.length.iota.map!(
		j => samples.map!(s => s.times[j]).filter!(t => t > 0).reduce!min / 1_000_000_000 - 1
	).array;

	foreach (i, row; samples)
	{
		if (i)
		{
			auto s = row.subject;
			s.skipOver("dustmite: ");
			s = row.commit[0..7] ~ " - " ~ s;
			// s = s.wrap(28);
			// auto lines = s.splitLines;
			// auto y0 = i * ROW_H + (ROW_H - lines.length * LABEL_LINE_H - COMMIT_HASH_H) / 2 + COMMIT_HASH_H;
			// commitHashes.text([
			// 	"x" : 0.text,
			// 	"y" : (y0).text,
			// ])[] = row.commit;
			// foreach (j, line; lines)
			// 	commitMessages.text([
			// 		"x" : 0.text,
			// 		"y" : (y0 + (1 + j) * LABEL_LINE_H).text,
			// 	])[] = line;

			enum commitMessageX = 50;
			enum commitMessagePad = 10;
			enum commitDelimLineY = -4;
			commitMessages.text([
				"x" : (commitMessageX).text,
				"y" : (LEGEND_H + i * ROW_H).text,
				"textLength" : (COMMIT_MESSAGE_CHAR_W * s.length).text,
			])[] = s;

			commitDelimiterPath ~= "M %d.5,%d.5 h %d M %d.5,%d.5 h %d".format(
				0                                                                   , LEGEND_H + i * ROW_H + commitDelimLineY, commitMessageX - commitMessagePad,
				commitMessageX + COMMIT_MESSAGE_CHAR_W * s.length + commitMessagePad, LEGEND_H + i * ROW_H + commitDelimLineY, SVG_W,
			);
		}

		writefln("%s", row.subject);
		foreach (j, t; row.times)
		{
			auto x0 = SVG_W / 2;
			auto xDir = [-1, 1][j];
			x0 += 5 * xDir;
			auto w = t / 1_000_000_000.0;
			writef("  t=%d w=%s min=%s w-min=%s log=%s ", t, w, mins[j], w - mins[j], log(w - mins[j]));
			// w = w <= 0 ? 0 : mins[j] * 5 + log(w - mins[j]) * 33;
			// w = w <= 0 ? 0 : log(w) * 43;
			w = w <= 0 ? 0 : w * 4;
			if (w > SVG_W / 2)
			{
				w = t / 1_000_000_000.0;
				w = log(w) * 43;
				tears.use([
					"xlink:href" : "#tear",
					"transform" : format!"translate(%d %d)"(
						x0 + SVG_W / 3 * xDir,
						LEGEND_H + i * ROW_H + 10,
					),
				]);
			}
			writefln("=> %s ", w);
			w *= xDir;
			auto x1 = x0 + w;
			times.rect([
				"x" : (min(x0, x1)).text,
				"y" : (LEGEND_H + i * ROW_H + 10).text,
				"width" : abs(w).text,
				"height" : (24).text,
				"fill" : ["url(#g1)", "url(#g2)"][j],
			]);
			auto s = t <= 0 ? "DNF" : t.text[0..$-9] ~ "." ~ t.text[$-9..$-8];
			timeLabels.text([
				"x" : (x0 + 5 * xDir).text,
				"y" : (LEGEND_H + i * ROW_H + 28).text,
				"text-anchor" : ["end", "start"][j],
				"fill" : t > 0 ? "#fff" : "#000",
			])[] = s;
		}
	}
	commitDelimiters["d"] = commitDelimiterPath;

	foreach (j; 0 .. 2)
	{
		times.rect([
			"x" : (j * (SVG_W / 2)).text,
			"y" : 0.text,
			"width" : (SVG_W / 2).text,
			"height" : (LEGEND_H).text,
			"fill" : ["url(#g1)", "url(#g2)"][j],
		]);

		auto xDir = [-1, 1][j];

		timeLabels.text([
			"x" : (SVG_W / 2 + 10 * xDir).text,
			"y" : (17).text,
			"text-anchor" : ["end", "start"][j],
			"fill" : "#fff",
		])[] = ["No lookahead (-j0)", "With lookahead (-j32)"][j];
	}

	svg.toString().toFile("times.svg");
}

// string[] wrapToLines(string s, size_t numLines)
// {
// 	for (int w = 1; w < s.length; w++)
// 	{
// 		auto lines = s.wrap(w).splitLines;
// 		if (lines.length <= numLines)
// 			return lines;
// 	}
// 	assert(false);
// }
